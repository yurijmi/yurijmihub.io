---
layout: post
title: WEBO Site SpeedUp Nulled
image: /images/webo.jpg
date: '2013-09-12 01:19:16 +0400'
date_gmt: '2013-09-11 21:19:16 +0400'
categories:
- proekty
- uroki
---
Всем привет! Я опять взялся за старое и снова занулил **WEBO Site SpeedUp** на этот уже все возможные модули версии **1.6.4**. На этот раз я расскажу, как **сделать нулл своими руками.**

## Как сделать WEBO Site SpeedUp Nulled самому?

А теперь я расскажу как можно самому занулить **WEBO Site SpeedUp**. Первым делом обратим внимание на замечательный файлик `libs/php/view.php`.

<span class="image fit">
![WEBO Site SpeedUp Nulled - view.php]({{ "/images/view.php.png" | absolute_url }})
</span>

Этот файл интересен тем, что в нем хранятся шаблоны, но нам сейчас не это важно. Идём на 200 строку, и видим...

<span class="image fit">
![WEBO Site SpeedUp Nulled - validate_license]({{ "/images/validate_license.png" | absolute_url }})
</span>

Джекпот! Функция проверки лицензии. Из-за неё и появился **WEBO Site SpeedUp Nulled**. Ну я думаю вы знаете что делать. Удалять всё и как можно скорее :).
А теперь минутка теории. После долго изучения <small>(примерно 2 минуты)</small> я понял как работает эта функция и что нужно изменять чтобы обойти проверку лицензии.

<span class="image fit">
![WEBO Site SpeedUp Nulled - versions]({{ "/images/versions.png" | absolute_url }})
</span>

Теперь вы знаете, что делать! Для тех, кто не хочет больше менять тип лицензии, мы закончили. **WEBO Site SpeedUp Nulled** готов ровно на 70%!

А теперь попробуем сделать вместо поля ввода лицензионного ключа простой выбор типа лицензии. Надеюсь вы еще не закрыли файл `libs/php/view.php`? Тогда пишем заместо проверки лицензии этот код:

```php
/**
* Validate license
* You can donate for Web Optimizer here: http://sprites.in/donate/
*
* Nulled Warning:
* 0 - no license / demo, 1 - standard edition,
* 2 - extended edition, 3 - corporate edition,
* 10 - SaaS license
**/
function validate_license ($license, $cachedir = false, $host = false) {
    return (is_numeric($license)) ? $license : 3;
}
```

Да, это код из моего нулла, но что он делает? Функция возвращает корпоративный тип лицензии, если в "ключе" нет "валидного" кода. Если он есть, функция его же и возвращает.
Легко, не правда ли? А теперь сделаем переключатель лицензии, чтобы каждый раз не вбивать 0, 1, 2, 3, 10 в поле лицензии.
Идем на 130 строку этого же файла (поэтому я назвал его замечательным), и видим шаблон `install_account`:

<span class="image fit">
![WEBO Site SpeedUp Nulled - install_account]({{ "/images/install_account.png" | absolute_url }})
</span>

Но ничего ведь не понятно, скажете Вы, а я Вам отвечу: [Tabifier](http://tools.arantius.com/tabifier) Вам в помощь! Только сильно с ним не играйтесь, рушит шаблон. Теперь, заменяем:

```php
<input id="wss_license" name="wss_license" title="<?php echo _WEBO_LOGIN_ENTERLICENSE; ?>" value="<?php if (empty($submit) || !empty($license)) { echo htmlspecialchars($license); } ?>" maxlength="29" size="29"><span>*</span><input type="hidden" name="wss_premium" id="wss_premium" value="<?php echo round($premium); ?>"> <span>-1 && $premium > 9) {  echo _WEBO_ACCOUNT_EXPIRES . ' ' . date("Y-m-d", time() + $expires*86400); ?>, [](javascript:(function(){var s=yass.doc.createElement('script');s.type='text/javascript';s.src='http://webo.name/license/trial/?name='+yass('#wss_name')[0].value+'&email='+yass('#wss_email')[0].value;yass('head')[0].appendChild(s)}())) ,[](<?php  echo $root; ?>LICENSE<?php  echo in_array($language, array('ru', 'ua')) ? '.utf8.ru' : ''; ?>.txt)</span>
```

На:

```php
<select id="wss_license" name="wss_license" title="<?php echo _WEBO_LOGIN_ENTERLICENSE; ?>" class="wssF"><option value="0" <?php="" echo="" (round($premium="" -="" 0.1)="=" 0)="" ?="" "selected"="" :="" "";="">></option> <option value="1" <?php="" echo="" (round($premium="" -="" 0.1)="=" 1)="" ?="" "selected"="" :="" "";="">></option> <option value="2" <?php="" echo="" (round($premium="" -="" 0.1)="=" 2)="" ?="" "selected"="" :="" "";="">></option> <option value="3" <?php="" echo="" (round($premium="" -="" 0.1)="=" 3)="" ?="" "selected"="" :="" "";="">></option> <option value="10" <?php="" echo="" (round($premium="" -="" 0.1)="=" 10)="" ?="" "selected"="" :="" "";="">></option> *</select><input type="hidden" name="wss_premium" id="wss_premium" value="<?php echo round($premium); ?>">
```

И получаем красивенькую (но не красивую в коде :() формочку для переключения лицензии на лету!

<span class="image fit">
![WEBO Site SpeedUp Nulled - переключатель типа лицензии]({{ "/images/fancy_select.png" | absolute_url }})
</span>

Спасибо за внимание, с Вами был сонный Yurijmi, надеюсь вам понравилось. Пишите в комментарии, какие уроки вы увидеть в моем исполнении. :)
