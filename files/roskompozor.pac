function FindProxyForURL(url, host) {
  if (
    dnsDomainIs(host, "baskino.co") ||
    dnsDomainIs(host, "f-list.net") ||
    dnsDomainIs(host, "inkbunny.net") ||
    dnsDomainIs(host, "4pna.com") ||
    dnsDomainIs(host, "blah.im") ||
    dnsDomainIs(host, "derpiboo.ru") ||
    dnsDomainIs(host, "derpibooru.org") ||
    dnsDomainIs(host, "derpicdn.net") ||
    dnsDomainIs(host, "e621.net") ||
    dnsDomainIs(host, "e-hentai.org") ||
    dnsDomainIs(host, "gidonline.club") ||
    dnsDomainIs(host, "gidonline.in") ||
    dnsDomainIs(host, "gidonline.pro") ||
    dnsDomainIs(host, "nnmclub.to") ||
    dnsDomainIs(host, "owlgraphic.com") ||
    dnsDomainIs(host, "rule34.paheal.net") ||
    dnsDomainIs(host, "tumblr.com") ||
    dnsDomainIs(host, "2ip.ru") ||
    dnsDomainIs(host, "rutracker.org") ||
    dnsDomainIs(host, "everypony.ru") ||
    dnsDomainIs(host, "lurkmore.to") ||
    dnsDomainIs(host, "nnm-club.me") ||
    dnsDomainIs(host, "nnmclub.to") ||
    dnsDomainIs(host, "nnm-club.name") ||
    dnsDomainIs(host, "rutor.org") ||
    dnsDomainIs(host, "rutor.info") ||
    dnsDomainIs(host, "rutracker.ru")
  ) {
    return "SOCKS5 fr11.friproxy.biz:1080";
  } else {
    return "DIRECT";
  }
}
